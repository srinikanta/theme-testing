import * as React from 'react';
import { NavGroupType } from './components/Nav';
import { NavToggler } from './components/Nav';
import { Panel, PanelType } from 'office-ui-fabric-react/lib/Panel';
import { ChoiceGroup } from 'office-ui-fabric-react/lib/ChoiceGroup';
import { 
  PrimaryButton, 
  DefaultButton, 
  ScreenWidthMinMedium,
  SwatchColorPicker 
} from 'office-ui-fabric-react';
import { Redirect, withRouter } from 'react-router-dom';

class Nav extends React.Component {
  state = {
    showPanel: false,
    color: undefined,
    previewColor: undefined,
    color2: undefined,
    previewColor2: undefined
  };
  constructor(props) {
    super(props);
  }

  render() {
    const navLinkGroups = [
      {
        links: [
          { name: 'Collapsed', alternateText: 'Expanded', url: '#', icon: 'GlobalNavButton', key: 'key' }
        ],
        groupType: NavGroupType.ToggleGroup
      },

      {
        links: [
          { name: 'Buttons', url: '#', onClick: this._navigateButtonsPage, icon: 'Home', key: 'key1' },
          { name: 'Dropdowns', url: '#', onClick: this._navigateDrodownsPage, icon: 'DietPlanNotebook', key: 'key2' },
          { name: 'Sliders', url: '#', onClick: this._navigateSlidersPage, icon: 'Page', key: 'key3' },
          { name: 'Datepickers', url: '#', onClick: this._navigateDatepickersPage, icon: 'Page', key: 'key4' },
          { name: 'ChoiceGroup', url: '#', onClick: this._navigateDatepickersPage, icon: 'Page', key: 'key5' },
          { name: 'Checkbox', url: '#', onClick: this._navigateDatepickersPage, icon: 'Page', key: 'key6' },
          { name: 'TextFields', url: '#', onClick: this._navigateDatepickersPage, icon: 'Page', key: 'key5' },
        ],
        groupType: NavGroupType.MenuGroup
      },
      {
        links: [
          { name: 'Settings', url: '#', onClick: this._showPanel, icon: 'Settings', key: 'key5' },
        ],
        groupType: NavGroupType.CustomizationGroup
      }
    ];
    const styles = {
      
    };
    return (
      <div>
        <NavToggler 
          groups={navLinkGroups} 
          dataHint="LeftNav" 
          enableCustomization={true} 
          selectedKey="key1"
        />
        <Panel
          isOpen={this.state.showPanel}
          type={PanelType.smallFixedFar}
          onDismiss={this._onClosePanel}
          headerText="Quick Settings"
          closeButtonAriaLabel="Close"
          onRenderFooterContent={this._onRenderFooterContent}
          styles={styles}
        >
          <div>Theme</div>
          <SwatchColorPicker
            columnCount={5}
            selectedId={this.state.color}
            cellShape={'circle'}
            onColorChanged={(id, color) => this.setState({ color: color })}
            colorCells={[
              { id: 'a', label: 'green', color: '#00ff00' },
              { id: 'b', label: 'orange', color: '#ffa500' },
              { id: 'c', label: 'blue', color: '#0000ff' },
              { id: 'd', label: 'red', color: '#ff0000' },
              { id: 'e', label: 'white', color: '#ffffff' }
            ]}
          />
        </Panel>
      </div>
    );
  }
  _onRenderFooterContent = () => {
    return (
      <div>
        <PrimaryButton onClick={this._onClosePanel} style={{ marginRight: '8px' }}>
          Save
        </PrimaryButton>
        <DefaultButton onClick={this._onClosePanel}>Cancel</DefaultButton>
      </div>
    );
  };
  _navigateDatepickersPage = () => {
    this.props.history.push('/datepickers')
  }
  _navigateSlidersPage = () => {
    this.props.history.push('/sliders')
  }
  _navigateDrodownsPage = () => {
    this.props.history.push('/dropdowns')
  }
  _navigateButtonsPage = () => {
    this.props.history.push('/buttons')
  }
  _onClosePanel = () => {
    this.setState({ showPanel: false });
  }
  _showPanel = () => {
    this.setState({ showPanel: true });
  }
}

export default withRouter(Nav);