import React, { Component } from "react";
import PropTypes from 'prop-types';
import Nav from './Nav.jsx';
import Routes from './Routes';
import './Header.scss';
import { FontClassNames } from 'office-ui-fabric-react/lib/Styling';

class Layout extends Component {
  state = {
    open: true,
    navOpen: false
  };

  render() {
    
    return (
      <div>
        <div className="ms-Grid" dir="ltr">
            <div className="ms-Grid-row">
                <div className="ms-Grid-col ms-sm6 ms-md4 ms-lg12">
                  <div className="Header">
                    <div className={'Header-title ' + FontClassNames.large}>Back Testing</div>
                  </div>
                </div>
            </div>
            <div className="ms-Grid-row">
                <div className="ms-Grid-col ms-sm6 ms-md4 ms-lg2"><Nav/></div>
                <div className="ms-Grid-col ms-sm6 ms-md8 ms-lg10">
                    <Routes/>
                </div>
            </div>
        </div>
      </div>
    );
  }
}
export default Layout;
