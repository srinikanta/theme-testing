import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { loadTheme } from 'office-ui-fabric-react';

import './styles/style.sass'

loadTheme({
  palette: {
    themePrimary: '#3caaff',
    themeLighterAlt: '#02070a',
    themeLighter: '#0a1b29',
    themeLight: '#12334d',
    themeTertiary: '#256799',
    themeSecondary: '#3696e0',
    themeDarkAlt: '#51b3ff',
    themeDark: '#6cbfff',
    themeDarker: '#92d0ff',
    neutralLighterAlt: '#3c3c3c',
    neutralLighter: '#444444',
    neutralLight: '#515151',
    neutralQuaternaryAlt: '#595959',
    neutralQuaternary: '#5f5f5f',
    neutralTertiaryAlt: '#7a7a7a',
    neutralTertiary: '#c8c8c8',
    neutralSecondary: '#d0d0d0',
    neutralPrimaryAlt: '#dadada',
    neutralPrimary: '#ffffff',
    neutralDark: '#f4f4f4',
    black: '#f8f8f8',
    white: '#333333',
  }
});
ReactDOM.render(
  <App />,
  document.getElementById('app')
)