import React, { lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';

const ButtonsPage = lazy(() => import('./components/Buttons/ButtonsPage'));
const DropdownPage = lazy(() => import('./components/Dropdowns/DropdownsPage'));
const SliderPage = lazy(() => import('./components/Sliders/SlidersPage'));
const DatepickersPage = lazy(() => import('./components/Datepickers/DatepickersPage'));
import PageLoader from './common/PageLoader';

const Routes = () => {
    return (
      <Suspense fallback={<PageLoader />}>
        <Switch>
          <Route
            exact
            path="/buttons"
            render={props => {
              return <ButtonsPage/>
            }}
          />
          <Route
            exact
            path="/dropdowns"
            render={props => {
              return <DropdownPage/>
            }}
          />
          <Route
            path="/sliders"
            render={props => {
              return <SliderPage />;
            }}
          />
          <Route
            exact
            path="/datepickers"
            render={props => {
              return <DatepickersPage/>
            }}
          />
        </Switch>
      </Suspense>
    );
  };
  

export default Routes;