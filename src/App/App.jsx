import React, { Component } from 'react'
import { initializeIcons } from '@uifabric/icons';
import { PrimaryButton } from 'office-ui-fabric-react/lib/Button';
import { BrowserRouter as Router } from 'react-router-dom';

import Layout from '../Layout';

// Register icons and pull the fonts from the default SharePoint CDN:
initializeIcons();

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <Layout/>
        </Router>
      </React.Fragment>
    )
  }
}

export default App