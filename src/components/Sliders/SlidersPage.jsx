import React, { Component } from 'react'
import { Slider } from 'office-ui-fabric-react';

class SlidersPage extends Component {
  state = {
    value: 0
  };
  render() {
    return (
      <React.Fragment>
        <Slider
            label="Basic example:"
            min={1}
            max={5}
            step={1}
            defaultValue={2}
            showValue={true}
            // tslint:disable-next-line:jsx-no-lambda
            onChange={(value) => console.log(value)}
          />
      </React.Fragment>
    )
  }
}

export default SlidersPage