import React, { Component } from 'react'
import { PrimaryButton, Dropdown  } from 'office-ui-fabric-react';

class DropdownsPage extends Component {
  state = {
    selectedItem: undefined,
    selectedItems: []
  };
  render() {
    const { selectedItem, selectedItems } = this.state;
    return (
      <React.Fragment>
        <Dropdown
          label="Controlled example:"
          selectedKey={selectedItem ? selectedItem.key : undefined}
          onChange={this.changeState}
          onFocus={this._log('onFocus called')}
          onBlur={this._log('onBlur called')}
          placeholder="Select an Option"
          options={[
            { key: 'A', text: 'Option a' },
            { key: 'B', text: 'Option b' },
            { key: 'C', text: 'Option c' }
          ]}
        />
      </React.Fragment>
    )
  }
  changeState = (event, item) => {
    console.log('here is the things updating...' + item.key + ' ' + item.text + ' ' + item.selected);
    this.setState({ selectedItem: item });
  };
  _log =  (str) =>  {
    return () => {
      console.log(str);
    };
  }
}

export default DropdownsPage