import React, { Component } from 'react'
import { PrimaryButton, DefaultButton, ContextualMenu, IconButton  } from 'office-ui-fabric-react';

class ButtonsPage extends Component {
  render() {
    return (
      <React.Fragment>
        <PrimaryButton>I am a button.</PrimaryButton>
        <br/><br/>
        <DefaultButton
          data-automation-id="test"
          allowDisabledFocus={true}
          disabled={false}
          checked={true}
          text="Button"
        />
        <br/><br/>
        <DefaultButton
            data-automation-id="test"
            disabled={false}
            checked={true}
            text="Create account"
            onClick={this.alertClicked}
            split={true}
            splitButtonAriaLabel={'See 2 sample options'}
            aria-roledescription={'split button'}
            style={{ height: '35px' }}
            menuProps={{
              items: [
                {
                  key: 'emailMessage',
                  text: 'Email message',
                  iconProps: { iconName: 'Mail' }
                },
                {
                  key: 'calendarEvent',
                  text: 'Calendar event',
                  iconProps: { iconName: 'Calendar' }
                }
              ]
            }}
          />
          <br/><br/>
          <DefaultButton
            primary
            data-automation-id="test"
            disabled={false}
            checked={true}
            text="Create account"
            onClick={this.alertClicked}
            split={true}
            aria-roledescription={'split button'}
            style={{ height: '35px' }}
            menuProps={{
              items: [
                {
                  key: 'emailMessage',
                  text: 'Email message',
                  iconProps: { iconName: 'Mail' }
                },
                {
                  key: 'calendarEvent',
                  text: 'Calendar event',
                  iconProps: { iconName: 'Calendar' }
                }
              ]
            }}
          />
          <br/><br/>
          <DefaultButton
            data-automation-id="test"
            disabled={false}
            allowDisabledFocus={true}
            checked={true}
            iconProps={{ iconName: 'Add' }}
            menuAs={this._getMenu}
            text="New"
            // tslint:disable-next-line:jsx-no-lambda
            onMenuClick={ev => {
              console.log(ev);
            }}
            menuProps={{
              items: [
                {
                  key: 'emailMessage',
                  text: 'Email message',
                  iconProps: { iconName: 'Mail' }
                },
                {
                  key: 'calendarEvent',
                  text: 'Calendar event',
                  iconProps: { iconName: 'Calendar' }
                }
              ],
              directionalHintFixed: true
            }}
          />
          <br/><br/>
          <IconButton
            primary={true}
            disabled={false} 
            checked={true} 
            iconProps={{ iconName: 'Emoji2' }} 
            title="Emoji" ariaLabel="Emoji" />
      </React.Fragment>
    )
  }
  alertClicked = () => {
    alert('Clicked');
  };
  _getMenu = (menuProps) => {
    // Customize contextual menu with menuAs
    return <ContextualMenu {...menuProps} />;
  };
}

export default ButtonsPage