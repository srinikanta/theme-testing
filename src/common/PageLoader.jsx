import React from 'react';
import PropTypes from 'prop-types';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';

const PageLoader = () => {
  return (
    <React.Fragment>
        <Spinner size={SpinnerSize.large} />
    </React.Fragment>
  );
};


export default PageLoader;